﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using SaberTest.BL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

namespace SaberTestWpf.Models
{
    public class RandomListModel : ViewModelBase
    {

        private ListRandom _currentListRandom;
        private FileSystemWatcher _fsw;

        public RandomListModel()
        {

            FileName = Path.Combine(Directory.GetCurrentDirectory(), "Nodes.dat");
            CurrentListRandom = new ListRandom();
            _fsw = new FileSystemWatcher(Path.GetDirectoryName(FileName)) { Filter = "*.dat" };
            _fsw.Created += SaveCreated;
            _fsw.Deleted += SaveDeleted;
            IsCanDeserialize = File.Exists(FileName);
            UpdateNodes();

        }

        private void SaveDeleted(object sender, FileSystemEventArgs e) => IsCanDeserialize = false;
        private void SaveCreated(object sender, FileSystemEventArgs e) => IsCanDeserialize = true;

        public int TemplateCount { get; set; } = 6;
        public bool IsCanDeserialize { get; private set; }
        public string FileName { get; set; }
        public ObservableCollection<ListNode> Nodes { get; } = new ObservableCollection<ListNode>();
        public ListRandom CurrentListRandom { get => _currentListRandom; set { Set(ref _currentListRandom, value); UpdateNodes(); } }


        private RelayCommand _updateValuesCommand;
        public RelayCommand UpdateValuesCommand => _updateValuesCommand ?? (_updateValuesCommand = new RelayCommand(UpdateNodes));
        private RelayCommand _serializeCommand;
        public RelayCommand SerializeCommand => _serializeCommand ?? (_serializeCommand = new RelayCommand(Serialize, Nodes.Count > 0));
        private RelayCommand _deserializeCommand;

        public RelayCommand DeserializeCommand => _deserializeCommand ?? (_deserializeCommand = new RelayCommand(Deserezlize, IsCanDeserialize));

        private RelayCommand _createTemplateCommand;

        public RelayCommand CreateTemplateCommand => _createTemplateCommand ?? (_createTemplateCommand = new RelayCommand(() => CreateTemplate(TemplateCount)));
        private RelayCommand _clearDataCommand;
        public RelayCommand ClearDataCommand => _clearDataCommand ?? (_clearDataCommand = new RelayCommand(ClearData));

        public void ClearData()
        {
            CurrentListRandom = new ListRandom();
            UpdateNodes();
        }

        public void Insert(string dataText, ListNode randomRef)
        {
            CurrentListRandom.Insert(CurrentListRandom.Tail, new ListNode { Data = dataText, Random = randomRef });
            UpdateNodes();
        }

        public void CreateTemplate(int listLength)
        {
            CurrentListRandom.Head = new ListNode() { Data = "Head" };
            CurrentListRandom.Tail = CurrentListRandom.Head;

            var rand = new Random();
            int randInt;
            for (int i = 1; i < listLength; i++)
            {
                CurrentListRandom.Insert(CurrentListRandom.Tail, new ListNode() { Data = $"node-{i}" });
            }

            List<ListNode> nodes = CurrentListRandom.GetEnumerator().ToList();
            foreach (var node in nodes)
            {
                randInt = rand.Next(0, listLength);
                node.Random = nodes[randInt];
            }
            UpdateNodes();
        }

        private void UpdateNodes()
        {
            Nodes.Clear();
            foreach (var node in CurrentListRandom.GetEnumerator())
            {
                Nodes.Add(node);
            }
        }

        private void Serialize()
        {
            try
            {
                using (var fs = new FileStream(FileName, FileMode.Create, FileAccess.Write))
                {
                    CurrentListRandom.Serialize(fs);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show($"Something going wrong\n{e.Message}");
            }
        }

        private void Deserezlize()
        {
            try
            {
                FileInfo fileInfo = new FileInfo(FileName);
                if (fileInfo.Exists)
                {
                    using (var fs = new FileStream(FileName, FileMode.Open, FileAccess.Read))
                    {
                        CurrentListRandom.Deserialize(fs);
                    }
                }
                else
                {
                    MessageBox.Show("File not found");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show($"Something going wrong:\n{e.Message}");
            }
            UpdateNodes();
        }
    }
}
