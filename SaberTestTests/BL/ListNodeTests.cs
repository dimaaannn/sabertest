﻿using SaberTest.BL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SaberTestTests.BL
{
    [TestClass()]
    public class ListNodeTests
    {
        private ListNode _listNode1;
        private ListNode _listNode2;
        private ListNode _listNode3;

        private const string FIRST = "first";
        private const string SECOND = "second";
        private const string THRID = "thrid";

        [TestInitialize()]
        public  void TestInit()
        {
            _listNode1 = new ListNode() { Data = FIRST };
            _listNode1.Next = _listNode2 = new ListNode() { Previous = _listNode1, Data = SECOND };
            _listNode2.Next = _listNode3 = new ListNode() {Previous= _listNode2, Data = THRID };

        }
        [TestMethod]
        public void CreateList()
        {
            Assert.AreEqual(FIRST, _listNode1.Data);
            Assert.AreEqual(SECOND, _listNode1.Next.Data);
            Assert.AreEqual(SECOND, _listNode3.Previous.Data);
        }

        [TestMethod]
        public void SerializeTest()
        {
            MemoryStream memstream = new System.IO.MemoryStream();
            var writer = new StreamWriter(memstream);



        }

        private static void SerializeNode(ListNode node, StreamWriter streamWriter)
        {

        }
    }
}
