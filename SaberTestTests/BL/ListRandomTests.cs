﻿using SaberTest.BL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;

namespace SaberTest.BL.Tests
{
    [TestClass()]
    public class ListRandomTests
    {
        private static ListRandom _listRandom;
        private static int _listLength = 11;
        private static FileStream _fileStream;

        [ClassInitialize()]
        public static void Initialize(TestContext testContext)
        {
            _listRandom = new ListRandom();
            _listRandom.Head = new ListNode() { Data = "node-0" };
            _listRandom.Tail = _listRandom.Head;

            var rand = new Random();
            int randInt;
            for(int i = 1; i < _listLength; i++)
            {
                _listRandom.Insert(_listRandom.Tail, new ListNode() { Data = $"node-{i}" });
            }

            List<ListNode> nodes = _listRandom.GetEnumerator().ToList();
            foreach (var node in nodes)
            {
                randInt = rand.Next(0, _listLength);
                node.Random = nodes[randInt];
            }


            _fileStream = new FileStream("testSerialize", FileMode.Create, FileAccess.ReadWrite);
        }
        [ClassCleanup()]
        public static void Cleanup()
        {
            _fileStream.Close();
        }

        [TestMethod]
        public void CreateListRandomTest()
        {
            var enumerator = _listRandom.GetEnumerator();
            int prevPosition = -1; 
            int nodePosition;
            foreach (var node in enumerator)
            {
                nodePosition = Int32.Parse(node.Data.Split('-').Last());
                StringAssert.Contains(node.Data, "node");
                Assert.IsTrue(prevPosition == nodePosition - 1);
                prevPosition = nodePosition;
            }
        }


        [TestMethod()]
        public void DeserializeTest()
        {
            _listRandom.Serialize(_fileStream);

            _fileStream.Position = 0;
            _listRandom = new ListRandom();

            _listRandom.Deserialize(_fileStream);
            IEnumerable<ListNode> enumerator = _listRandom.GetEnumerator();
            var list = enumerator.ToList();

            int enumCount = enumerator.Count();
            Assert.IsTrue(enumCount == _listLength);

            int prevPosition = -1;
            int nodePosition;
            foreach (var node in enumerator)
            {
                nodePosition = Int32.Parse(node.Data.Split('-').Last());
                StringAssert.Contains(node.Data, "node");
                Assert.IsTrue(prevPosition == nodePosition - 1);
                prevPosition = nodePosition;

                Assert.IsNotNull(node.Random.Data);
            }
        }

    }
}